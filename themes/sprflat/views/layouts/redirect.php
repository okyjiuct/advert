<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="/static/css/bootstrap.min.css">
        <link rel="icon" href="/favicon.ico" type="image/png">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <script language='javascript'>
            var delay = 0;
            setTimeout("document.location.href='<?= $this->_url; ?>'", delay);
        </script>
        <style>
            #circularG {
                position: relative;
                width: 128px;
                height: 128px;
                margin: 0 auto;
            }

            .circularG {
                position: absolute;
                background-color: #5EB6FE;
                width: 29px;
                height: 29px;
                -moz-border-radius: 19px;
                -moz-animation-name: bounce_circularG;
                -moz-animation-duration: 0.48s;
                -moz-animation-iteration-count: infinite;
                -moz-animation-direction: normal;
                -webkit-border-radius: 19px;
                -webkit-animation-name: bounce_circularG;
                -webkit-animation-duration: 0.48s;
                -webkit-animation-iteration-count: infinite;
                -webkit-animation-direction: normal;
                -ms-border-radius: 19px;
                -ms-animation-name: bounce_circularG;
                -ms-animation-duration: 0.48s;
                -ms-animation-iteration-count: infinite;
                -ms-animation-direction: normal;
                -o-border-radius: 19px;
                -o-animation-name: bounce_circularG;
                -o-animation-duration: 0.48s;
                -o-animation-iteration-count: infinite;
                -o-animation-direction: normal;
                border-radius: 19px;
                animation-name: bounce_circularG;
                animation-duration: 0.48s;
                animation-iteration-count: infinite;
                animation-direction: normal;
            }

            #circularG_1 {
                left: 0;
                top: 50px;
                -moz-animation-delay: 0.18s;
                -webkit-animation-delay: 0.18s;
                -ms-animation-delay: 0.18s;
                -o-animation-delay: 0.18s;
                animation-delay: 0.18s;
            }

            #circularG_2 {
                left: 14px;
                top: 14px;
                -moz-animation-delay: 0.24s;
                -webkit-animation-delay: 0.24s;
                -ms-animation-delay: 0.24s;
                -o-animation-delay: 0.24s;
                animation-delay: 0.24s;
            }

            #circularG_3 {
                top: 0;
                left: 50px;
                -moz-animation-delay: 0.3s;
                -webkit-animation-delay: 0.3s;
                -ms-animation-delay: 0.3s;
                -o-animation-delay: 0.3s;
                animation-delay: 0.3s;
            }

            #circularG_4 {
                right: 14px;
                top: 14px;
                -moz-animation-delay: 0.36s;
                -webkit-animation-delay: 0.36s;
                -ms-animation-delay: 0.36s;
                -o-animation-delay: 0.36s;
                animation-delay: 0.36s;
            }

            #circularG_5 {
                right: 0;
                top: 50px;
                -moz-animation-delay: 0.42s;
                -webkit-animation-delay: 0.42s;
                -ms-animation-delay: 0.42s;
                -o-animation-delay: 0.42s;
                animation-delay: 0.42s;
            }

            #circularG_6 {
                right: 14px;
                bottom: 14px;
                -moz-animation-delay: 0.48s;
                -webkit-animation-delay: 0.48s;
                -ms-animation-delay: 0.48s;
                -o-animation-delay: 0.48s;
                animation-delay: 0.48s;
            }

            #circularG_7 {
                left: 50px;
                bottom: 0;
                -moz-animation-delay: 0.54s;
                -webkit-animation-delay: 0.54s;
                -ms-animation-delay: 0.54s;
                -o-animation-delay: 0.54s;
                animation-delay: 0.54s;
            }

            #circularG_8 {
                left: 14px;
                bottom: 14px;
                -moz-animation-delay: 0.6s;
                -webkit-animation-delay: 0.6s;
                -ms-animation-delay: 0.6s;
                -o-animation-delay: 0.6s;
                animation-delay: 0.6s;
            }

            @-moz-keyframes bounce_circularG {
                0% {
                    -moz-transform: scale(1)
                }
                100% {
                    -moz-transform: scale(.3)
                }
            }

            @-webkit-keyframes bounce_circularG {
                0% {
                    -webkit-transform: scale(1)
                }
                100% {
                    -webkit-transform: scale(.3)
                }
            }

            @-ms-keyframes bounce_circularG {
                0% {
                    -ms-transform: scale(1)
                }
                100% {
                    -ms-transform: scale(.3)
                }
            }

            @-o-keyframes bounce_circularG {
                0% {
                    -o-transform: scale(1)
                }
                100% {
                    -o-transform: scale(.3)
                }
            }

            @keyframes bounce_circularG {
                0% {
                    transform: scale(1)
                }
                100% {
                    transform: scale(.3)
                }
            }

            .page-header {
                border: none;
            }

            .container {
                margin-top: 100px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div id="circularG">
                <div id="circularG_1" class="circularG">
                </div>
                <div id="circularG_2" class="circularG">
                </div>
                <div id="circularG_3" class="circularG">
                </div>
                <div id="circularG_4" class="circularG">
                </div>
                <div id="circularG_5" class="circularG">
                </div>
                <div id="circularG_6" class="circularG">
                </div>
                <div id="circularG_7" class="circularG">
                </div>
                <div id="circularG_8" class="circularG">
                </div>
            </div>

            <div class="page-header">
                <h1 class="text-center">Вы перенаправляетесь к объявлению. Пожалуйста, подождите</h1>
            </div>
            <p class="text-center">При возникновении ошибки Вы можете вернуться к <a href="/">поиску</a></p>
        </div>
    </body>
</html>
