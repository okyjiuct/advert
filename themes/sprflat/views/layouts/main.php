<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?=Y::yandex();?>
        <?=Y::google();?>
        <link rel="stylesheet" href="/static/css/bootstrap.css?v=<?=Y::getFiletime('/static/css/bootstrap.css');?>">
        <link rel="icon" href="/favicon.ico" type="image/png">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body>
        <div class="container">
            <div class="row marketing main">
                <?php echo $content; ?>
            </div>
        </div>
        <?=Y::analytics();?>
        <?=Y::metrika();?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="/static/js/bootstrap.min.js?v=<?=Y::getFiletime('/static/js/bootstrap.min.js');?>"></script>
    </body>
    <!-- <?=Y::stats();?> -->
</html>;