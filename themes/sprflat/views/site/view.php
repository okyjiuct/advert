<?php

$itemCount = $widget->dataProvider->itemCount;

// блок перед последним объявлением
if ($index == 1 && $itemCount > 5) {
    echo Yii::app()->params['center'];
}

// блок после первого объявления
if ($index == 0 && $itemCount > 5) {
    echo Yii::app()->params['top'];
}

?>

<div class="item">
    <div class="col-xs-4 text-center">
        <img src="<?= $data->img; ?>" title="<?= $data->title . ' - ' . $data->city . ', ' . $data->region; ?>"
             alt="<?= $data->title . ' - ' . $data->city . ', ' . $data->region; ?>" class="mbottom12"
             style="max-width: 100%;"/>
        <div>
            <strong class="price"><?= $data->price; ?></strong>
        </div>
    </div>
    <div class="col-xs-8">
        <h4 class="h4-title" data-hash="<?= $data->hash; ?>"><?= $data->title; ?></h4>
        <div class="mtop12 small">
            <?= ($data->category != $data->city) ? '<i class="fa fa-tags orange"></i> ' . $data->category : ''; ?>
        </div>
        <p class="mtop12 text"><?= $data->text; ?></p>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-4 col-sm-4 col-xs-4 mtop12 small">
        <div class="text-center"><?= $data->author ? '<i class="fa fa-user orange"></i> ' . $data->author : ''; ?></div>
    </div>
    <div class="col-md-5 col-sm-5 col-xs-8 mtop12 small">
        <i class="fa fa-map-marker orange"></i> <?= $data->city . ', ' . $data->region; ?>
    </div>
    <div class="col-md-3 col-sm-3 hidden-xs mtop12 small">
        <i class="fa fa-cloud orange"></i> <?= Yii::t('app', 'searchOn', ['{source}' => 'avito.ru']); ?>
    </div>
</div>

<div class="clearfix"></div>