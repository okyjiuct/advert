<h1 class="text-center"><?= Yii::t('app', 'searchTitle', ['{theme}' => $settings->keys, '{query}' => $_GET['query']]) . (isset($_GET['region']) ? ' для региона ' . $_GET['region'] : ''); ?></h1>

<form class="form-horizontal" action="/" method="get">
    <div class="form-group col-md-10">
        <input placeholder="<?= Yii::t('app', 'find'); ?>" class="form-control" name="q" value=""
               type="text"/>
    </div>
    <div class="form-group col-md-2">
        <button class="btn btn-success btn-search" type="submit"><?= Yii::t('app', 'search'); ?></button>
    </div>
</form>

<div class="clearfix"></div>

<div class="row">

    <?php

    $prevNext = '<div class="row">
        <h4 class="col-md-6 text-left">
            <a class="btn btn-default btn-sm" href="' . Y::url('/site/query', ['query' => $prevQuery->query]) . '">&#8249;&#8249; ' . $prevQuery->query . '</a>
        </h4>
        <h4 class="col-md-6 text-right">
            <a class="btn btn-default btn-sm" href="' . Y::url('/site/query', ['query' => $nextQuery->query]) . '">' . $nextQuery->query . ' &#8250;&#8250;</a>
        </h4></div>';

    $topAds = $settings->top;
    $bottomAds = $settings->bottom;

    $page = (int)Yii::app()->request->getQuery('Ads_page', 1);

    // если на страницах нет объявлений - редирект на первую
    if (empty($dataProvider->getData())) {

        if ($page > 1) {
            $this->redirect(Y::url('/site/query', ['query' => $_GET['query']]), true, 301);
        }
        $topAds = '';
        $bottomAds = '';
    }

    // если заход на страницу далее 3 - редирект на первую
    if ($page > 3) {
        $this->redirect(Y::url('/site/query', ['query' => $_GET['query']]), true, 301);
    }

    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => 'view',
        'ajaxUpdate' => false,
        'cssFile' => false,
        'template' => "<div class='col-md-12'>" . $topAds . "<div class='clearfix'></div>{items}<div class='clearfix'></div>" . $bottomAds . "<div class='clearfix'></div>" . $prevNext . "<div class='text-center'>{pager}</div></div>",
        'pager' => array(
            'maxButtonCount' => '5',
            'prevPageLabel' => Yii::t('app', 'prevPageLabel'),
            'firstPageLabel' => Yii::t('app', 'firstPageLabel'),
            'nextPageLabel' => Yii::t('app', 'nextPageLabel'),
            'lastPageLabel' => Yii::t('app', 'lastPageLabel'),
            'header' => '',
            'htmlOptions' => array('class' => 'pagination pagination-sm pull-left push-down-20'),
            'firstPageCssClass' => 'hidden', //default "first"
            'lastPageCssClass' => 'hidden', //default "last"
            'previousPageCssClass' => 'hidden', //default "previours"
            'nextPageCssClass' => 'hidden', //default "next"
            'internalPageCssClass' => '', //default "page"
            'selectedPageCssClass' => 'active', //default "selected"
            'hiddenPageCssClass' => '', //default "hidden"
            'cssFile' => false
        ),
        'pagerCssClass' => 'pager pagination'
    ));
    ?>
</div>

