<form class="form-horizontal" action="/" method="get">
    <div class="form-group col-md-10">
        <input placeholder="<?= Yii::t('app', 'find'); ?>" class="form-control" name="q" value=""
               type="text"/>
    </div>
    <div class="form-group col-md-2">
        <button class="btn btn-success btn-search" type="submit"><?= Yii::t('app', 'search'); ?></button>
    </div>
</form>

<div class="clearfix"></div>

<div class="row">
    <h3 class="text-center"><?= Yii::t('app', 'allQueries'); ?> | <a href="/"><?= Yii::t('app', 'main'); ?></a></h3>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => 'all_view',
        'ajaxUpdate' => false,
        'cssFile' => false,
        'template' => "<div class='col-md-12'><div class='text-center'>{pager}</div><div class='clearfix'></div><div class='row'>{items}</div><div class='clearfix'></div><div class='text-center'>{pager}</div></div>",
        'pager' => array(
            'maxButtonCount' => '5',
            'prevPageLabel' => Yii::t('app', 'prevPageLabel'),
            'firstPageLabel' => Yii::t('app', 'firstPageLabel'),
            'nextPageLabel' => Yii::t('app', 'nextPageLabel'),
            'lastPageLabel' => Yii::t('app', 'lastPageLabel'),
            'header' => '',
            'htmlOptions' => array('class' => 'pagination pagination-sm pull-left push-down-20'),
            'firstPageCssClass' => 'hidden', //default "first"
            'lastPageCssClass' => 'hidden', //default "last"
            'previousPageCssClass' => 'hidden', //default "previours"
            'nextPageCssClass' => 'hidden', //default "next"
            'internalPageCssClass' => '', //default "page"
            'selectedPageCssClass' => 'active', //default "selected"
            'hiddenPageCssClass' => '', //default "hidden"
            'cssFile' => false
        ),
        'pagerCssClass' => 'pager pagination'
    ));
    ?>
</div>
