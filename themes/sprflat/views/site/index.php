<form class="form-horizontal" action="/" method="get">
    <div class="form-group col-md-10">
        <input placeholder="<?= Yii::t('app', 'find'); ?>" class="form-control" name="q" value=""
               type="text"/>
    </div>
    <div class="form-group col-md-2">
        <button class="btn btn-success btn-search" type="submit"><?= Yii::t('app', 'search'); ?></button>
    </div>
</form>

<div class="clearfix"></div>

<div class="row">
    <h5 class="text-center">
        <a data-toggle="collapse" href="#lastQueries" aria-expanded="false"
           aria-controls="lastQueries">
            <?= Yii::t('app', 'lastQueries'); ?>
        </a>
    </h5>

    <div class="collapse" id="lastQueries">
        <div class="col-md-12 queries">
            <?php foreach ($lastQueries as $item) : ?>
                <div class="query">
                    <a href="<?= Y::url('/site/query', ['query' => $item->query]) ; ?>"><?= $item->query; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                </div>
            <?php endforeach; ?>
        </div>
        <h5 class="text-center mtop"><a href="<?= Y::url('/site/all') ; ?>"><?= Yii::t('app', 'allQueries'); ?></a></h5>
    </div>

    <h5 class="text-center">
        <a data-toggle="collapse" href="#about" aria-expanded="false"
           aria-controls="about">
            <?= Yii::t('app', 'about'); ?>
        </a>
    </h5>

    <div class="collapse" id="about">
        <div class="col-md-12 queries">
            <?= Yii::t('app', 'description', ['{theme}' => Y::settings()->keys]); ?>
        </div>
    </div>
</div>