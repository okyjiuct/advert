<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'settings-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
    )); ?>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'slug', array('for' => "slug")); ?>
        <?php echo $form->textField($model, 'slug', array('class' => 'form-control', 'id' => 'slug')); ?>
        <?php echo $form->error($model, 'slug'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'keys', array('for' => "keys")); ?>
        <?php echo $form->textField($model, 'keys', array('class' => 'form-control', 'id' => 'keys')); ?>
        <?php echo $form->error($model, 'keys'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'metrika', array('for' => "metrika")); ?>
        <?php echo $form->textField($model, 'metrika', array('class' => 'form-control', 'id' => 'metrika')); ?>
        <?php echo $form->error($model, 'metrika'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'yandex', array('for' => "yandex")); ?>
        <?php echo $form->textField($model, 'yandex', array('class' => 'form-control', 'id' => 'yandex')); ?>
        <?php echo $form->error($model, 'yandex'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'analytics', array('for' => "analytics")); ?>
        <?php echo $form->textField($model, 'analytics', array('class' => 'form-control', 'id' => 'analytics')); ?>
        <?php echo $form->error($model, 'analytics'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'google', array('for' => "google")); ?>
        <?php echo $form->textField($model, 'google', array('class' => 'form-control', 'id' => 'google')); ?>
        <?php echo $form->error($model, 'google'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'query', array('for' => "query")); ?>
        <?php echo $form->textField($model, 'query', array('class' => 'form-control', 'id' => 'query')); ?>
        <?php echo $form->error($model, 'query'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'top', array('for' => "top")); ?>
        <?php echo $form->textArea($model, 'top', array('class' => 'form-control', 'id' => 'top', 'rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'top'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'center', array('for' => "center")); ?>
        <?php echo $form->textArea($model, 'center', array('class' => 'form-control', 'id' => 'center', 'rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'center'); ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model, 'bottom', array('for' => "bottom")); ?>
        <?php echo $form->textArea($model, 'bottom', array('class' => 'form-control', 'id' => 'bottom', 'rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'bottom'); ?>
    </div>

    <div class="buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', array('class' => 'btn btn-success')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->