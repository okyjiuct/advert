<?php
/**
 * Created by PhpStorm.
 * User: Kohone
 * Date: 23.04.2015
 * Time: 20:01
 */

class PingCommand extends CConsoleCommand
{
    public function run($args)
    {
        Yii::app()->cache->flush();
        Y::sendSitemap();
    }
}