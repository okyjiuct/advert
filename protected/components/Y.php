<?php

include(Yii::getPathOfAlias('ext.SimpleHTMLDOM.simple_html_dom') . '.php');

class Y {

    /**
     * @var array Кэш компонентов приложения
     * @since 1.2.0
     */
    private static $_componentsCache = array();

    /**
     * Парсим страницу и возвращаем Simple HTML DOM объект
     * @param $href
     * @return mixed
     */
    public static function getPage($href, $referer = false) {
        $proxy = Proxy::getProxy();

        if ($proxy !== false) {
            fopen('cookie.txt', 'w+');

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $href);

            if (!$referer) {
                curl_setopt($curl, CURLOPT_REFERER, $href);
            } else {
                curl_setopt($curl, CURLOPT_REFERER, $referer);
            }

            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 6);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, Y::userAgent());
            curl_setopt($curl, CURLOPT_PROXY, "{$proxy['ip']}");
            curl_setopt($curl, CURLOPT_PROXYUSERPWD, "{$proxy['user']}");
            curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookie.txt'); // сохранять куки в файл
            curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookie.txt');
            $str = curl_exec($curl);
            curl_close($curl);

            $dom = str_get_html($str);

            $needle = 'ограничен';
            $pos = strripos($dom, $needle);
            if ($pos !== false) {
                Proxy::changeStatusProxy($proxy['id']);
            }

            return $dom;
        }

        return false;
    }

    /**
     * Ярлык для функции dump класса CVarDumper для отладки приложения
     * @param mixed $var Переменная для вывода
     * @param boolean $doEnd Остановить ли дальнейшее выполнение приложения, по умолчанию - true
     */
    public static function dump($var, $doEnd = true) {
        echo '<pre>';
        CVarDumper::dump($var, 10, true);
        echo '</pre>';

        if ($doEnd) {
            Yii::app()->end();
        }
    }

    /**
     * Получаем хеш
     *
     * @param $data
     * @return string
     */
    public static function getHash($data = null) {
        if ($data)
            return hash('crc32b', strrev(md5($data)));

        return hash('crc32b', strrev(md5(time() . rand(100000, 9999999))));
    }

    /**
     * Обрезаем строку
     *
     * @param $string
     * @param int $length
     * @return string
     */
    public static function cut($string, $length = 400) {
        if (mb_strlen($string) < $length)
            return $string;
        $string = strip_tags($string);
        $string = mb_substr($string, 0, $length);
        $string = rtrim($string, "!,.-");
        $string = mb_substr($string, 0, strrpos($string, ' '));

        return $string . "...";
    }

    public static function seo($query) {
        $keywords = Y::keywords($query);

        Yii::app()->clientScript->registerMetaTag($keywords . Y::pageString(), 'keywords');
        Yii::app()->clientScript->registerMetaTag('Результаты поиска по запросу ' . $query . ' среди объявлений по всей России. Удобный сервис поиска объявлений о продаже ' . Y::settings()->keys . Y::pageString(), 'description');
    }

    public static function keywords($query) {
        $words = explode(" ", $query);

        $keywords = [$query];
        foreach ($words as $word) {
            $keywords [] = $word;
            $keywords [] = 'найти ' . $word;
            $keywords [] = 'поиск ' . $word;
            $keywords [] = $word . ' бу';
            $keywords [] = 'купить ' . $word . ' бу';
        }

        return implode(", ", $keywords);
    }

    /**
     * Получаем "Каждый с каждым" из массивов
     * @param $A
     * @param int $i
     * @return array
     */
    public static function variations($A, $i = 0) {
        $result = array();

        if ($i < count($A)) {
            $variations = Y::variation($A, $i + 1);

            for ($j = 0; $j < count($A[$i]); $j++) {
                if ($variations) {
                    foreach ($variations as $variation) {
                        $result[] = array_merge(array($A[$i][$j]), $variation);
                    }
                } else {
                    $result[] = array($A[$i][$j]);
                }
            }
        }

        return $result;
    }

    /**
     * Выводит статистику использованных приложением ресурсов
     * @param boolean $return Определяет возвращать результат или сразу выводить
     * @return string
     */
    public static function stats($return = false) {
        $stats = '';

        $criteria = new CDbCriteria();
        $countQuery = Query::model()->cache(60)->count($criteria);

        $criteria = new CDbCriteria();
        $countAds = Ads::model()->cache(60)->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->select = ['date'];
        $criteria->order = 'id DESC';
        $lastAd = Ads::model()->cache(60)->find($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = 'error = 1';
        $proxyError = Proxy::model()->cache(60)->count($criteria);

        $stats .= "\r\nЗапросов пользователей: " . $countQuery . " .\r\n";
        $stats .= "Объявлений: " . $countAds . " .\r\n";
        if ($lastAd)
            $stats .= "Последнее объявление: " . date("d-m-Y H:i", $lastAd->date) . " .\r\n";
        $stats .= "Прокси с ошибками: " . $proxyError . " .\r\n";

        if ($return) {
            return $stats;
        }

        echo $stats;
    }

    public static function userAgent() {
        $userAgents = array(
            'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
            'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1623.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.17 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36',
            'Mozilla/5.0 (X11; CrOS i686 4319.74.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1467.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1464.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0',
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0',
            'Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.2; rv:22.0) Gecko/20130405 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20130328 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20130405 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/21.0.1',
            'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:21.0.0) Gecko/20121011 Firefox/21.0.0',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; GTB7.4; InfoPath.2; SV1; .NET CLR 3.3.69573; WOW64; en-US)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 1.0.3705; .NET CLR 1.1.4322)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; InfoPath.1; SV1; .NET CLR 3.8.36217; WOW64; en-US)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; .NET CLR 2.7.58687; SLCC2; Media Center PC 5.0; Zune 3.4; Tablet PC 3.6; InfoPath.3)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; SLCC1; .NET CLR 1.1.4322)',
            'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.0; Trident/4.0; InfoPath.1; SV1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 3.0.04506.30)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.0; Trident/4.0; FBSMTWB; .NET CLR 2.0.34861; .NET CLR 3.0.3746.3218; .NET CLR 3.5.33652; msn OptimizedIE8;ENUS)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.2; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; Media Center PC 6.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.3; .NET4.0C; .NET4.0E; .NET CLR 3.5.30729; .NET CLR 3.0.30729; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.2)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Zune 3.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; msn OptimizedIE8;ZHCN)',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; el-GR)',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 5.2)',
            'Mozilla/5.0 (MSIE 7.0; Macintosh; U; SunOS; X11; gu; SV1; InfoPath.2; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; c .NET CLR 3.0.04506; .NET CLR 3.5.30707; InfoPath.1; el-GR)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; c .NET CLR 3.0.04506; .NET CLR 3.5.30707; InfoPath.1; el-GR)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; fr-FR)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.2; WOW64; .NET CLR 2.0.50727)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows 98; SpamBlockerUtility 6.3.91; SpamBlockerUtility 6.2.91; .NET CLR 4.1.89;GB)',
            'Mozilla/4.79 [en] (compatible; MSIE 7.0; Windows NT 5.0; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',
            'Mozilla/4.0 (Windows; MSIE 7.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)',
            'Mozilla/4.0 (Mozilla/4.0; MSIE 7.0; Windows NT 5.1; FDM; SV1; .NET CLR 3.0.04506.30)',
            'Mozilla/4.0 (Mozilla/4.0; MSIE 7.0; Windows NT 5.1; FDM; SV1)',
            'Mozilla/4.0 (compatible;MSIE 7.0;Windows NT 6.0)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.2; Win64; x64; Trident/6.0; .NET4.0E; .NET4.0C)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; SLCC2; .NET CLR 2.0.50727; InfoPath.3; .NET4.0C; .NET4.0E; .NET CLR 3.5.30729; .NET CLR 3.0.30729; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E; InfoPath.3)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; chromeframe/12.0.742.100)',
            'Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14',
            'Mozilla/5.0 (Windows NT 6.0; rv:2.0) Gecko/20100101 Firefox/4.0 Opera 12.14',
            'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0) Opera 12.14',
            'Opera/12.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.02',
            'Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00',
            'Opera/9.80 (Windows NT 5.1; U; zh-sg) Presto/2.9.181 Version/12.00',
            'Opera/12.0(Windows NT 5.2;U;en)Presto/22.9.168 Version/12.00',
            'Opera/12.0(Windows NT 5.1;U;en)Presto/22.9.168 Version/12.00',
            'Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/14.0 Opera/12.0',
            'Opera/9.80 (Windows NT 6.1; WOW64; U; pt) Presto/2.10.229 Version/11.62',
            'Opera/9.80 (Windows NT 6.0; U; pl) Presto/2.10.229 Version/11.62',
            'Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52',
            'Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; de) Presto/2.9.168 Version/11.52',
            'Opera/9.80 (Windows NT 5.1; U; en) Presto/2.9.168 Version/11.51',
            'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; de) Opera 11.51',
            'Opera/9.80 (X11; Linux x86_64; U; fr) Presto/2.9.168 Version/11.50',
            'Opera/9.80 (X11; Linux i686; U; hu) Presto/2.9.168 Version/11.50',
            'Opera/9.80 (X11; Linux i686; U; ru) Presto/2.8.131 Version/11.11',
            'Opera/9.80 (X11; Linux i686; U; es-ES) Presto/2.8.131 Version/11.11',
            'Mozilla/5.0 (Windows NT 5.1; U; en; rv:1.8.1) Gecko/20061208 Firefox/5.0 Opera 11.11',
            'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10',
            'Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko ) Version/5.1 Mobile/9B176 Safari/7534.48.3',
        );

        $rand = array_rand($userAgents);

        return $userAgents[$rand];
    }

    static public function pageString($param = 'Ads_page') {
        $page = (int) Yii::app()->request->getQuery($param, 1);

        return $page > 1 ? ' - страница ' . $page : '';
    }

    /**
     * Возвращает URL, сформированный на основе указанного маршрута и параметров
     * @param string $route Маршрут
     * @param array $params Дополнительные параметры маршрута
     * @return string
     */
    public static function url($route, $params = array()) {
        if (is_object($controller = Yii::app()->getController())) {
            return $controller->createUrl($route, $params);
        }

        return Yii::app()->createUrl($route, $params);
    }

    /**
     * Возвращает cache-компонент приложения
     * @param string $cacheId ID кэш-компонента (@since 1.1.3)
     * @return ICache
     */
    public static function cache($cacheId = 'cache') {
        return self::_getComponent($cacheId);
    }

    /**
     * Удаляет кэш с ключом $id
     * @param string $id Имя ключа
     * @param string $cacheId ID кэш-компонента (@since 1.1.3)
     * @return boolean
     */
    public static function cacheDelete($id, $cacheId = 'cache') {
        return self::_getComponent($cacheId)->delete($id);
    }

    /**
     * Возвращает значение кэша с ключом $id
     * @param string $id Имя ключа
     * @param string $cacheId ID кэш-компонента (@since 1.1.3)
     * @return mixed
     */
    public static function cacheGet($id, $cacheId = 'cache') {
        return self::_getComponent($cacheId)->get($id);
    }

    /**
     * Сохраняет значение $value в кэш с ключом $id на время $expire (в секундах)
     * @param string $id Имя ключа
     * @param mixed $value Значение ключа
     * @param integer $expire Время хранения в секундах
     * @param ICacheDependency $dependency Смотри {@link ICacheDependency}
     * @param string $cacheId ID кэш-компонента (@since 1.1.3)
     * @return boolean
     */
    public static function cacheSet($id, $value, $expire = 600, $dependency = null, $cacheId = 'cache') {
        return self::_getComponent($cacheId)->set($id, $value, $expire, $dependency);
    }

    /**
     *
     */
    public static function cacheFlush($cacheId = 'cache') {
        return self::_getComponent($cacheId)->flush();
    }

    /**
     * Возвращает компонтент приложения
     * Экономит лишние вызовы методов для получения компонентов путем кэширования компонентов
     * @param string $componentName Имя компонента приложения
     * @return CComponent
     * @since 1.2.0
     */
    private static function _getComponent($componentName) {
        if (!isset(self::$_componentsCache[$componentName])) {
            self::$_componentsCache[$componentName] = Yii::app()->getComponent($componentName);
        }

        return self::$_componentsCache[$componentName];
    }

    /**
     * Пингуем карту сайта
     * @param $url
     * @return mixed|string
     */
    public static function sendUrl($url) {
        if (function_exists('curl_init')) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6);

            $data = curl_exec($ch);
            curl_close($ch);

            return $data;
        } else {

            return @file_get_contents($url);
        }
    }

    /**
     * Пингуем ПС
     */
    public static function sendSitemap() {
        $map_link = Yii::app()->request->hostInfo . '/sitemap.xml';

        Y::sendUrl("http://google.com/webmasters/sitemaps/ping?sitemap=" . $map_link);

        Y::sendUrl("http://ping.blogs.yandex.ru/ping?sitemap=" . $map_link);

        Y::sendUrl("http://www.bing.com/webmaster/ping.aspx?siteMap=" . $map_link);

        Y::sendUrl("http://rpc.weblogs.com/pingSiteForm?name=InfraBlog&url=" . $map_link);
    }

    public static function getFiletime($path) {
        $dir = dirname(Yii::app()->basePath);

        return Y::getHash(filemtime($dir . $path));
    }

    public static function settings() {

        $cache_id = 'settings_cache';

        $settings = Yii::app()->cache->get($cache_id);
        if ($settings === false) {

            $settings = Settings::model()->findByPk(1);
            Yii::app()->cache->set($cache_id, $settings, 86400);
        }

        return $settings;
    }

    /**
     * Яндекс-вебмастер
     * @return type
     */
    public static function yandex() {
        $settings = Y::settings();

        if ($settings->yandex) {
            return '<meta name="yandex-verification" content="' . Y::settings()->yandex . '"/>' . "\n";
        }
    }

    /**
     * Гугл-вебмастер
     * @return type
     */
    public static function google() {
        $settings = Y::settings();

        if ($settings->google) {
            return '<meta name="google-site-verification" content="' . Y::settings()->google . '"/>' . "\n";
        }
    }

    /**
     * Яндекс Метрика
     * @return type
     */
    public static function metrika() {
        $settings = Y::settings();

        if ($settings->metrika) {
            return '<script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter' . $settings->metrika . ' = new Ya.Metrika({id:' . $settings->metrika . ', webvisor:true, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/' . $settings->metrika . '" style="position:absolute; left:-9999px;" alt="" /></div></noscript>' . "\n";
        }
    }
    
    /**
     * Гугл Аналитикс
     * @return type
     */
    public static function analytics() {
        $settings = Y::settings();

        if ($settings->analytics) {
            return "<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', '" . $settings->analytics . "', 'auto');ga('send', 'pageview');</script>" . "\n";
        }
    }

}
