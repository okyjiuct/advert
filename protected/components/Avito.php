<?php

class Avito
{
    public static function getAdvert()
    {
        $region = Region::getRegion();
        $mobHost = 'https://m.avito.ru/';
        $webHost = 'https://www.avito.ru';
        $settings = Settings::model()->findByPk(1);

        $url = $mobHost . $region->slug . '/' . $settings->slug . '/?user=1';

        $html = Y::getPage($url, $mobHost . $region->slug);

        if ($html !== false) {
            // получили страницу с объявлениями
            $result = $html->find('.b-item');

            $count = 0;
            foreach ($result as $element) {

                // спим перед парсингом
                usleep(mt_rand(3000000, 5000000));

                $data = [
                    'link' => '',
                    'mobLink' => '',
                    'hash' => '',
                    'shop' => '',
                    'title' => '',
                    'price' => '',
                    'city' => '',
                    'region' => '',
                    'metro' => '',
                    'img' => '',
                    'category' => '',
                    'text' => '',
                    'author' => '',
                    'date' => '',
                ];

                $data['region'] = $region->name;

                // link
                foreach ($element->find('.item-link') as $el) {
                    $data['link'] = $webHost . trim($el->href);
                    $data['mobLink'] = $mobHost . trim($el->href);

                    $data['hash'] = Y::getHash($data['link']);
                    // если уже парсили - пропускаем
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'hash = :hash';
                    $criteria->params = [':hash' => $data['hash']];
                    $exists = Ads::model()->exists($criteria);
                }
                if ($exists || $data['link'] == '') {
                    continue;
                }

                // shop - фильтруем магазины
                foreach ($element->find('.info-shop') as $el) {
                    $data['shop'] = trim($el->plaintext);
                }
                if ($data['shop'] != '') {
                    continue;
                }

                // title
                foreach ($element->find('.header-text') as $el) {
                    $data['title'] = trim($el->plaintext);
                }
                if ($data['title'] == '') {
                    continue;
                }

                // каждый 50-й запрос сохраняем как введенный юзером


                $rand = mt_rand(1, $settings->query);
                if ($rand == 1) {
                    // добавляем новый запрос в базу
                    Query::newQuery($data['title']);
                }

                // price
                foreach ($element->find('.item-price') as $el) {
                    $price = trim($el->plaintext);
                    $data['price'] = trim(str_replace("&nbsp;", " ", $price));
                }
                if ($data['price'] == '') {
                    continue;
                }

                // city
                if ($region->slug == 'moskva' || $region->slug == 'sankt-peterburg') {
                    $data['city'] = $region->name;
                    foreach ($element->find('.info-metro-district') as $el) {
                        $metro = trim($el->plaintext);
                        $data['metro'] = trim(str_replace("&nbsp;", " ", $metro));
                    }
                } else {
                    foreach ($element->find('.info-location') as $el) {
                        $data['metro'] = '';
                        $city = trim($el->plaintext);
                        $data['city'] = trim(str_replace("&nbsp;", " ", $city));
                    }
                }
                if ($data['city'] == '') {
                    continue;
                }

                // image
                foreach ($element->find('.pseudo-img') as $el) {
                    $image = trim($el->style);
                    preg_match("/\((.*)\)/i", $image, $found);
                    if (isset($found[1])) {
                        $data['img'] = $found[1];
                    }
                }
                if ($data['img'] == '') {
                    continue;
                }

                // парсим полное объявление
                $adHtml = Y::getPage($data['mobLink'], $url);

                if ($adHtml == false) {
                    continue;
                }

                //category
                $data['category'] = [];
                foreach ($adHtml->find('.param') as $el) {
                    $data['category'][] = trim($el->plaintext);
                }
                $count_el = sizeof($data['category']);
                if(sizeof($data['category']) > 1) {
                    $data['category'] = $data['category'][$count_el - 1] . ' - ' . $data['category'][$count_el - 2];
                } else {
                    $data['category'] = $data['category'][$count_el - 1];
                }

                // text
                foreach ($adHtml->find('.description-preview-wrapper') as $el) {
                    $data['text'] = trim($el->plaintext);
                }

                // author
                foreach ($adHtml->find('.person-name') as $el) {
                    $data['author'] = trim($el->plaintext);
                }

                // date
                $data['date'] = time();

                $ad = new Ads;
                $ad->attributes = $data;

                if ($ad->save()) {
                    $count++;
                }
            }

            // удаляем объявления старше месяца
//            $date = time() - 30 * 24 * 60 * 60;
//            $criteria = new CDbCriteria();
//            $criteria->condition = 'date < :date';
//            $criteria->params = array(':date' => $date);
//            $result = Ads::model()->deleteAll($criteria);

            echo "Add $count ads\r\n";

            unset($ad);
            unset($data);
            unset($result);
        } else {
            echo "Parse error\r\n";
        }

        unset($html);
    }
}