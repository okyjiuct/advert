<?php

/**
 * Created by PhpStorm.
 * User: Kohone
 * Date: 01.03.2015
 * Time: 11:03
 */
class SitemapController extends Controller
{
    public function actionIndex()
    {
        $urls = array();

        $cacheId = Y::getHash('sitemap_' . Yii::app()->params['yandex']);
        $sitemap = Y::cacheGet($cacheId);

        if (!$sitemap) {
            $sitemap = Query::model()->findAll();
            Y::cacheSet($cacheId, $sitemap, 6 * 60 * 60);
        }

        foreach ($sitemap as $query) {
            $urls[] = $query->query;
        }

        $host = Yii::app()->request->hostInfo;

        header("Content-type: text/xml");
        echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($urls as $query) {
            echo '<url>
                <loc>' . $host . Y::url('/site/query', ['query' => htmlspecialchars($query)]) . '</loc>
                <changefreq>daily</changefreq>
                <priority>0.5</priority>
            </url>';
        }
        echo '</urlset>';
        Yii::app()->end();
    }
}