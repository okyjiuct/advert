<?php

class SiteController extends Controller
{
    protected $_url;
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            array(
                'application.filters.YXssFilter',
                'clean' => 'all'
            )
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'error', 'all', 'query', 'add', 'r', 'addProxy', 'cache', 'ping', 'test'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Поиск ' . Yii::app()->params['keys'] . ' на досках объявлений';

        if (isset($_GET['query'])) $this->redirect(Y::url('/site/query', ['query' => $_GET['query']]), true, 301);
        if (isset($_GET['q'])) $this->redirect(Y::url('/site/query', ['query' => $_GET['q']]), true, 301);

        $lastQueries = Query::lastQueries();
        $this->render('index', ['lastQueries' => $lastQueries]);
    }


    /**
     * Обработка запросов
     * @param $query
     */
    public function actionQuery($query)
    {
        $this->pageTitle = $query;
        Y::seo($query);

        $this->layout = 'search';

        $dataProvider = new CActiveDataProvider(Ads::model()->cache(60 * 60), array(
            'criteria' => array(
                'condition' => "MATCH (title, region, city) AGAINST(:keyword)",
                'params' => array(
                    ':keyword' => $query,
                ),
                'limit' => 75,
                //'order' => 'date DESC'
            ),
            'pagination' => array(
                'pageSize' => 25,
            ),
            'totalItemCount' => 75,
        ));

        $data = $dataProvider->getData();
        // если есть результаты поиска
        if (!empty($data)) {
            // закрываем от индексации все страницы, кроме первой
            $currentPage = $dataProvider->pagination->currentPage + 1;
            if ($currentPage > 1) {
                Yii::app()->clientScript->registerMetaTag('noindex, nofollow', 'robots');
            }
            
            $base_url = str_replace('http://', 'https://', Yii::app()->getBaseUrl(true));

            Yii::app()->clientScript->registerLinkTag('canonical', null, $base_url . Y::url('/site/query', ['query' => $query]));
        }

        $randomQueries = Query::randomQuery($query);

        $settings = Settings::model()->findByPk(1);

        $this->render('result', array(
            'dataProvider' => $dataProvider,
            'randomQueries' => $randomQueries,
            'prevQuery' => Query::prevQuery($query),
            'nextQuery' => Query::nextQuery($query),
            'settings' => $settings,
        ));
    }

    public function actionAll()
    {
        $this->layout = 'search';

        $query = 'Запросы, заданные пользователями.';

        $this->pageTitle = $query;
        Y::seo($query);

        $dataProvider = new CActiveDataProvider(Query::model()->cache(60 * 60),
            array(
                'criteria' => array(
                    'order' => 'id DESC',
                ),
                'pagination' => array(
                    'pageSize' => '100'
                )
            )
        );

        $settings = Settings::model()->findByPk(1);

        $this->render('all', array(
            'dataProvider' => $dataProvider,
            'settings' => $settings
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else {
                $this->pageTitle = $error['message'];
                $this->render('error', $error);
            }
        }
    }

    public function actionAdd()
    {
        $list = [];

        foreach ($list as $item) {
            Query::newQuery($item);
        }
    }

    /**
     * Редирект внешних ссылок
     */
    public function actionR($hash)
    {
        $link = Ads::findByHash($hash);
        if ($link == null) $this->redirect('/', true, 301);
        else {
            $this->pageTitle = 'Перенаправление';

            $this->layout = 'redirect';
            $this->_url = $link->link;

            $this->render('r');
        }
    }

    public function actionAddProxy()
    {
        $list = [];

        foreach ($list as $proxy) {
            Proxy::newProxy($proxy);
        }
    }

    public function actionCache()
    {
        Yii::app()->cache->flush();
        $this->redirect('/', true);
    }

    public function actionPing()
    {
        Y::sendSitemap();
        $this->redirect('/', true);
    }


    public function actionTest()
    {

    }
}
