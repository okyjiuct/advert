<?php

class SettingsController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'search';

    public function filters()
    {
        return array(
            'accessControl', //access control filter should be applied to every action of the controller
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform these actions
                'actions' => array('update', 'index'),
//                'ips' => array("185.37.192.59", "185.37.193.142", "162.158.91.199", "162.158.90.202", "141.101.104.78")
            ),
            array('deny', //deny by default
                'ips' => array("*")
            ),
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        if(!isset($_GET['pass']) || $_GET['pass'] != '5092503') {
            $this->redirect('/');
        }

        $model = $this->loadModel($id);

        if (isset($_POST['Settings'])) {
            $model->attributes = $_POST['Settings'];
            if ($model->save()) {
                Yii::app()->cache->flush();

                $this->redirect(array('update', 'id' => $model->id, 'pass' => '5092503'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = Settings::model()->findByPk(1);

        if(!isset($_GET['pass']) || $_GET['pass'] != '5092503') {
            $this->redirect('/');
        }

        $this->redirect(array('update', 'id' => $model->id, 'pass' => '5092503'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Settings the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Settings::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        return $model;
    }

}
