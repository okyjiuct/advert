<?php

class m160218_191817_change_settings extends CDbMigration {

    public function up() {
        $this->execute('ALTER TABLE `settings` ADD `analytics` VARCHAR(255) NOT NULL AFTER `yandex`, ADD `google` VARCHAR(255) NOT NULL AFTER `analytics`;');
    }

    public function down() {
        echo "m160218_191817_change_settings does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
