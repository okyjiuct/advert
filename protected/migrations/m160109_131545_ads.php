<?php

class m160109_131545_ads extends CDbMigration
{
    public function up()
    {

        $this->execute('CREATE TABLE `ads` (
          `id` int(11) NOT NULL,
          `title` text COLLATE utf8_unicode_ci NOT NULL,
          `text` text COLLATE utf8_unicode_ci NOT NULL,
          `link` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
          `img` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
          `price` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
          `hash` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
          `date` int(11) NOT NULL,
          `category` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
          `region` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
          `city` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
          `metro` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
          `author` varchar(32) COLLATE utf8_unicode_ci NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        ALTER TABLE `ads`
          ADD PRIMARY KEY (`id`),
          ADD UNIQUE KEY `hash` (`hash`);
        ALTER TABLE `ads` ADD FULLTEXT KEY `title_2` (`title`,`region`,`city`);

        ALTER TABLE `ads` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

');



    }

    public function down()
    {
        echo "m160109_131545_ads does not support migration down.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}