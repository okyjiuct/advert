<?php

class m160109_132537_proxy extends CDbMigration
{
    public function up()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `proxy` (
            `id` int(11) NOT NULL,
            `ip_port` varchar(32) CHARACTER SET utf8 NOT NULL,
            `login_pass` varchar(64) CHARACTER SET utf8 NOT NULL,
            `error` int(1) NOT NULL,
            `used` tinyint(1) NOT NULL,
            `created_at` int(11) NOT NULL,
            `updated_at` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

            ALTER TABLE `proxy` ADD PRIMARY KEY (`id`);

            ALTER TABLE `proxy` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

";
        Yii::app()->db2->createCommand($sql)->execute();
    }

    public function down()
    {
        echo "m160109_132537_proxy does not support migration down.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}