<?php

class m160402_093206_primary_key_in_regions extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE `region` ADD PRIMARY KEY (`id`);');
	}

	public function down()
	{
		echo "m160402_093206_primary_key_in_regions does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}