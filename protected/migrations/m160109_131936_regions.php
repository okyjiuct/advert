<?php

class m160109_131936_regions extends CDbMigration
{
    public function up()
    {
        $this->execute('CREATE TABLE `region` (
          `id` int(11) NOT NULL,
          `slug` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
          `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
          `used` tinyint(4) NOT NULL DEFAULT \'0\'
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');

        $this->execute('INSERT INTO `region` (`id`, `slug`, `name`, `used`) VALUES
            (1, \'moskva\', \'Москва\', 1),
            (2, \'moskovskaya_oblast\', \'Московская обл.\', 1),
            (3, \'sankt-peterburg\', \'Санкт-Петербург\', 1),
            (4, \'leningradskaya_oblast\', \'Ленинградская обл.\', 1),
            (5, \'adygeya\', \'Адыгея\', 1),
            (6, \'altayskiy_kray\', \'Алтайский край\', 1),
            (7, \'amurskaya_oblast\', \'Амурская обл.\', 1),
            (8, \'arhangelskaya_oblast\', \'Архангельская обл.\', 1),
            (9, \'astrahanskaya_oblast\', \'Астраханская обл.\', 1),
            (10, \'bashkortostan\', \'Башкортостан\', 1),
            (11, \'belgorodskaya_oblast\', \'Белгородская обл.\', 1),
            (12, \'bryanskaya_oblast\', \'Брянская обл.\', 1),
            (13, \'buryatiya\', \'Бурятия\', 1),
            (14, \'vladimirskaya_oblast\', \'Владимирская обл.\', 1),
            (15, \'volgogradskaya_oblast\', \'Волгоградская обл.\', 1),
            (16, \'vologodskaya_oblast\', \'Вологодская обл.\', 1),
            (17, \'voronezhskaya_oblast\', \'Воронежская обл.\', 1),
            (18, \'dagestan\', \'Дагестан\', 1),
            (19, \'evreyskaya_ao\', \'Еврейская АО\', 1),
            (20, \'zabaykalskiy_kray\', \'Забайкальский край\', 1),
            (21, \'ivanovskaya_oblast\', \'Ивановская обл.\', 1),
            (22, \'ingushetiya\', \'Ингушетия\', 1),
            (23, \'irkutskaya_oblast\', \'Иркутская обл.\', 1),
            (24, \'kabardino-balkariya\', \'Кабардино-Балкария\', 1),
            (25, \'kaliningradskaya_oblast\', \'Калининградская обл.\', 0),
            (26, \'kalmykiya\', \'Калмыкия\', 0),
            (27, \'kaluzhskaya_oblast\', \'Калужская обл.\', 0),
            (28, \'kamchatskiy_kray\', \'Камчатский край\', 0),
            (29, \'karachaevo-cherkesiya\', \'Карачаево-Черкесия\', 0),
            (30, \'kareliya\', \'Карелия\', 0),
            (31, \'kemerovskaya_oblast\', \'Кемеровская обл.\', 0),
            (32, \'kirovskaya_oblast\', \'Кировская обл.\', 0),
            (33, \'komi\', \'Коми\', 0),
            (34, \'kostromskaya_oblast\', \'Костромская обл.\', 0),
            (35, \'krasnodarskiy_kray\', \'Краснодарский край\', 0),
            (36, \'krasnoyarskiy_kray\', \'Красноярский край\', 0),
            (37, \'krym\', \'Крым\', 0),
            (38, \'kurganskaya_oblast\', \'Курганская обл.\', 0),
            (39, \'kurskaya_oblast\', \'Курская обл.\', 0),
            (40, \'lipetskaya_oblast\', \'Липецкая обл.\', 0),
            (41, \'magadanskaya_oblast\', \'Магаданская обл.\', 0),
            (42, \'mariy_el\', \'Марий Эл\', 0),
            (43, \'mordoviya\', \'Мордовия\', 0),
            (44, \'murmanskaya_oblast\', \'Мурманская обл.\', 0),
            (45, \'nenetskiy_ao\', \'Ненецкий АО\', 0),
            (46, \'nizhegorodskaya_oblast\', \'Нижегородская обл.\', 0),
            (47, \'novgorodskaya_oblast\', \'Новгородская обл.\', 0),
            (48, \'novosibirskaya_oblast\', \'Новосибирская обл.\', 0),
            (49, \'omskaya_oblast\', \'Омская обл.\', 0),
            (50, \'orenburgskaya_oblast\', \'Оренбургская обл.\', 0),
            (51, \'orlovskaya_oblast\', \'Орловская обл.\', 0),
            (52, \'penzenskaya_oblast\', \'Пензенская обл.\', 0),
            (53, \'permskiy_kray\', \'Пермский край\', 0),
            (54, \'primorskiy_kray\', \'Приморский край\', 0),
            (55, \'pskovskaya_oblast\', \'Псковская обл.\', 0),
            (56, \'respublika_altay\', \'Республика Алтай\', 0),
            (57, \'rostovskaya_oblast\', \'Ростовская обл.\', 0),
            (58, \'ryazanskaya_oblast\', \'Рязанская обл.\', 0),
            (59, \'samarskaya_oblast\', \'Самарская обл.\', 0),
            (60, \'saratovskaya_oblast\', \'Саратовская обл.\', 0),
            (61, \'sahalinskaya_oblast\', \'Сахалинская обл.\', 0),
            (62, \'saha_yakutiya\', \'Саха (Якутия)\', 0),
            (63, \'sverdlovskaya_oblast\', \'Свердловская обл.\', 0),
            (64, \'severnaya_osetiya\', \'Северная Осетия\', 0),
            (65, \'smolenskaya_oblast\', \'Смоленская обл.\', 0),
            (66, \'stavropolskiy_kray\', \'Ставропольский край\', 0),
            (67, \'tambovskaya_oblast\', \'Тамбовская обл.\', 0),
            (68, \'tatarstan\', \'Татарстан\', 0),
            (69, \'tverskaya_oblast\', \'Тверская обл.\', 0),
            (70, \'tomskaya_oblast\', \'Томская обл.\', 0),
            (71, \'tulskaya_oblast\', \'Тульская обл.\', 0),
            (72, \'tyva\', \'Тыва\', 0),
            (73, \'tyumenskaya_oblast\', \'Тюменская обл.\', 0),
            (74, \'udmurtiya\', \'Удмуртия\', 0),
            (75, \'ulyanovskaya_oblast\', \'Ульяновская обл.\', 0),
            (76, \'habarovskiy_kray\', \'Хабаровский край\', 0),
            (77, \'hakasiya\', \'Хакасия\', 0),
            (78, \'hanty-mansiyskiy_ao\', \'Ханты-Мансийский АО\', 0),
            (79, \'chelyabinskaya_oblast\', \'Челябинская обл.\', 0),
            (80, \'chechenskaya_respublika\', \'Чеченская республика\', 0),
            (81, \'chuvashiya\', \'Чувашия\', 0),
            (82, \'chukotskiy_ao\', \'Чукотский АО\', 0),
            (83, \'yamalo-nenetskiy_ao\', \'Ямало-Ненецкий АО\', 0),
            (84, \'yaroslavskaya_oblast\', \'Ярославская обл.\', 0);');

    }

    public function down()
    {
        echo "m160109_131936_regions does not support migration down.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}