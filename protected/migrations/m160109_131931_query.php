<?php

class m160109_131931_query extends CDbMigration
{
    public function up()
    {
        $this->execute('CREATE TABLE `query` (
          `id` int(11) NOT NULL,
          `query` text COLLATE utf8_unicode_ci NOT NULL,
          `hash` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
          `date` int(11) NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

        ALTER TABLE `query`
          ADD PRIMARY KEY (`id`),
          ADD UNIQUE KEY `hash` (`hash`);
        ALTER TABLE `query` ADD FULLTEXT KEY `query` (`query`);

        ALTER TABLE `query` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

');
    }

    public function down()
    {
        echo "m160109_131931_query does not support migration down.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}