<?php

class m160109_141130_settings extends CDbMigration
{
    public function up()
    {
        $this->createTable('settings', array(
            'id' => 'pk',
            'slug' => 'string NOT NULL',
            'keys' => 'string NOT NULL',
            'top' => 'text',
            'center' => 'text',
            'bottom' => 'text',
            'metrika' => 'string',
            'yandex' => 'string',
            'query' => 'string',
        ));

        $this->execute('INSERT INTO `settings` (`id`, `slug`, `keys`, `top`, `center`, `bottom`, `metrika`, `yandex`, `query`) VALUES
            (1, \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'50\');');

    }

    public function down()
    {
        echo "m160109_141130_settings does not support migration down.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}