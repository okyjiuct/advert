<?php

return array(
    'class' => 'system.db.CDbConnection',
    'connectionString' => 'mysql:host=localhost;dbname=db_name',
    'enableProfiling' => !YII_DEBUG,
    'enableParamLogging' => YII_DEBUG,
    'username' => 'sites',
    'password' => '5092503',
    'charset' => 'utf8',
    'schemaCachingDuration' => 3600,
);
