<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    'id' => 'adverts-avito',
    'theme' => 'sprflat',
    'language' => 'ru',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        'gii' => require(dirname(__FILE__) . '/gii.php'),
    ),
    // application components
    'components' => array(
        'clientScript' => array(
            //'class' => "ext.minScript.components.ExtMinScript",
            'enableJavaScript' => false,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '' => 'site/index',
                '<action:(all)>' => 'site/<action>',
                '<action:(query)>/<query:.+>' => 'site/<action>',
                'sitemap.xml' => 'sitemap/index',
                '<action:(r)>/<hash:\w+>' => 'site/<action>',
            ),
        ),
        'db' => require(dirname(__FILE__) . '/database.php'),
        'db2' => require(dirname(__FILE__) . '/proxy.php'),
        'cache' => array(
            'class' => 'CFileCache',
        ),
        'user' => array(
            'behaviors' => array(
                'botRecognizer' => array(
                    'class' => 'application.extensions.botRecognizer.botRecognizer',
                )
            ),
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    /*
    'controllerMap' => array(
        'min' => array(
            'class' => "ext.minScript.controllers.ExtMinScriptController",
        )
    ),*/
    // using Yii::app()->params['paramName']
    'params' => array(),
);
