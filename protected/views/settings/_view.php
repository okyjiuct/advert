<?php
/* @var $this SettingsController */
/* @var $data Settings */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?>:</b>
	<?php echo CHtml::encode($data->slug); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keys')); ?>:</b>
	<?php echo CHtml::encode($data->keys); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('top')); ?>:</b>
	<?php echo CHtml::encode($data->top); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('center')); ?>:</b>
	<?php echo CHtml::encode($data->center); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bottom')); ?>:</b>
	<?php echo CHtml::encode($data->bottom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('metrika')); ?>:</b>
	<?php echo CHtml::encode($data->metrika); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('yandex')); ?>:</b>
	<?php echo CHtml::encode($data->yandex); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('query')); ?>:</b>
	<?php echo CHtml::encode($data->query); ?>
	<br />

	*/ ?>

</div>