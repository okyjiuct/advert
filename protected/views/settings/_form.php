<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'slug'); ?>
		<?php echo $form->textField($model,'slug',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'slug'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keys'); ?>
		<?php echo $form->textField($model,'keys',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'keys'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'top'); ?>
		<?php echo $form->textArea($model,'top',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'top'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'center'); ?>
		<?php echo $form->textArea($model,'center',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'center'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bottom'); ?>
		<?php echo $form->textArea($model,'bottom',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'bottom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'metrika'); ?>
		<?php echo $form->textField($model,'metrika',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'metrika'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yandex'); ?>
		<?php echo $form->textField($model,'yandex',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yandex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'query'); ?>
		<?php echo $form->textField($model,'query',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'query'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->