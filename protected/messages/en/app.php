<?php
/**
 * Created by PhpStorm.
 * User: Kohone
 * Date: 29.03.2015
 * Time: 17:25
 */

return [
    'search' => 'Search',
    'region' => 'Region',
    'about' => 'About us',
    'lastQueries' => 'Last queries',
    'allQueries' => 'Full list of queries',
    'find' => 'Find a...',
    'description' => 'With our help, you can find much easier ads to buy or sell {theme} to the UK.',
    'searchTitle' => '<a href="/" title="Home">Search</a> {theme} on query "{query}"',
    'relatedQueries' => 'Related queries',
    'prevPageLabel' => 'Back',
    'firstPageLabel' => 'First',
    'nextPageLabel' => 'Forward',
    'lastPageLabel' => 'Last',
    'main' => 'Main',
    'returnBack' => 'Back',
    'price' => 'Price',
    'searchOn' => 'Search on {source}'
];