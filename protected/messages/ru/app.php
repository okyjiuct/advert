<?php
/**
 * Created by PhpStorm.
 * User: Kohone
 * Date: 29.03.2015
 * Time: 17:25
 */

return [
    'search' => 'Найти',
    'region' => 'Регион',
    'about' => 'О нас',
    'lastQueries' => 'Последние запросы пользователей',
    'allQueries' => 'Полный список запросов пользователей',
    'find' => 'Что ищем?',
    'description' => 'С нашей помощью вы значительно проще сможете найти объявления о покупке или продаже {theme} по территории России.',
    'searchTitle' => '<a href="/" title="На главную">Поиск</a> объявлений по запросу "{query}"',
    'relatedQueries' => 'Похожие запросы',
    'prevPageLabel' => 'Назад',
    'firstPageLabel' => 'Первая',
    'nextPageLabel' => 'Вперед',
    'lastPageLabel' => 'Последняя',
    'main' => 'Главная',
    'returnBack' => 'Вернуться назад',
    'price' => 'Цена',
    'searchOn' => 'Найдено на {source}',
    'randomQueries' => 'Также ищут',
];