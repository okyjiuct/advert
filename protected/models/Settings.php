<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $slug
 * @property string $keys
 * @property string $top
 * @property string $center
 * @property string $bottom
 * @property string $metrika
 * @property string $yandex
 * @property string $query
 */
class Settings extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('slug, keys', 'required'),
            array('slug, keys, metrika, yandex, analytics, google, query', 'length', 'max' => 255),
            array('top, center, bottom', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, slug, keys, top, center, bottom, metrika, yandex, analytics, google, query', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'slug' => 'URL категории',
            'keys' => 'Ключ для тайтлов',
            'top' => 'Блок верхней рекламы',
            'center' => 'Блок центральной рекламы',
            'bottom' => 'Блок нижней рекламы',
            'metrika' => 'Счетчик Метрики',
            'yandex' => 'Яндекс Вебмастер',
            'analytics' => 'Счетчик Google Analytics',
            'google' => 'Гугл Вебмастер',
            'query' => 'Каждое n-ое объявление в запрос',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('keys', $this->keys, true);
        $criteria->compare('top', $this->top, true);
        $criteria->compare('center', $this->center, true);
        $criteria->compare('bottom', $this->bottom, true);
        $criteria->compare('metrika', $this->metrika, true);
        $criteria->compare('yandex', $this->yandex, true);
        $criteria->compare('analytics', $this->analytics, true);
        $criteria->compare('google', $this->google, true);
        $criteria->compare('query', $this->query, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Settings the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
