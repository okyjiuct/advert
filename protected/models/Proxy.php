<?php

/**
 * This is the model class for table "proxy".
 *
 * The followings are the available columns in table 'proxy':
 * @property integer $id
 * @property string $ip
 * @property integer $port
 * @property string $login
 * @property string $pass
 */
class Proxy extends CActiveRecord {

    public function getDbConnection() {
        return Yii::app()->db2;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'proxy';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ip, login, pass', 'required'),
            array('port', 'numerical', 'integerOnly' => true),
            array('ip', 'length', 'max' => 15),
            array('login, pass', 'length', 'max' => 32),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, ip, port, login, pass', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'ip' => 'Ip',
            'port' => 'Port',
            'login' => 'Login',
            'pass' => 'Pass',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('port', $this->port);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('pass', $this->pass, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Proxy the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Получаем случайный прокси
     * @return array
     */
    public static function getProxy() {
        Proxy::proxyNotError();

        $proxy = Proxy::findProxy();

        if ($proxy !== null) {
            Proxy::updateProxy($proxy->id);

            return [
                'ip' => $proxy->ip_port,
                'user' => $proxy->login_pass,
                'id' => $proxy->id
            ];
        }

        Proxy::updateProxy();

        return false;
    }

    /**
     * Получаем для работы прокси
     * @return array|CActiveRecord|mixed|null
     */
    public static function findProxy() {
        $criteria = new CDbCriteria();
        $criteria->condition = 'used=0 AND error=0';

        return Proxy::Model()->find($criteria);
    }

    /**
     * Отмечаем, что прокси отработала, либо обнуляем все, если все отработали
     * @param null $id
     */
    public static function updateProxy($id = null) {
        if ($id) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'id = :id';
            $criteria->params = array(':id' => $id);
            $attributes = array(
                'used' => 1,
                'updated_at' => time(),
            );
            Proxy::model()->updateAll($attributes, $criteria);
        } else {
            $criteria = new CDbCriteria();
            $attributes = array(
                'used' => 0
            );
            Proxy::model()->updateAll($attributes, $criteria);
        }
    }

    /**
     * Отмечаем, что прокси заблокирована
     * @param $id
     */
    public static function changeStatusProxy($id) {
        $criteria = new CDbCriteria();
        $criteria->condition = 'id = :id';
        $criteria->params = array(':id' => $id);
        $attributes = array(
            'error' => 1,
            'updated_at' => time(),
        );
        Proxy::model()->updateAll($attributes, $criteria);
    }

    /**
     * Возвращаем ошибочные прокси в работу
     */
    public static function proxyNotError() {
        $date = time() - 6 * 60 * 60;

        $criteria = new CDbCriteria();
        $criteria->condition = 'updated_at < :updated_at';
        $criteria->params = array(':updated_at' => $date);
        $attributes = array(
            'error' => 0,
        );
        Proxy::model()->updateAll($attributes, $criteria);
    }

}
