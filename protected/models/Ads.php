<?php

/**
 * This is the model class for table "ads".
 *
 * The followings are the available columns in table 'ads':
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $link
 * @property string $img
 * @property string $price
 * @property string $hash
 * @property integer $date
 * @property string $category
 * @property string $region
 * @property string $city
 */
class Ads extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ads';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, text, link, img, price, hash, date, category, region, city', 'required'),
            array('date', 'numerical', 'integerOnly' => true),
            array('link, img', 'length', 'max' => 256),
            array('price', 'length', 'max' => 16),
            array('author', 'length', 'max' => 32),
            array('metro', 'length', 'max' => 64),
            array('hash', 'length', 'max' => 8),
            array('hash', 'unique'),
            array('category, region, city', 'length', 'max' => 128),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, text, link, img, price, hash, date, category, region, city, metro, author', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'link' => 'Link',
            'img' => 'Img',
            'price' => 'Price',
            'hash' => 'Hash',
            'date' => 'Date',
            'category' => 'Category',
            'region' => 'Region',
            'city' => 'City',
            'author' => 'Author',
            'metro' => 'Metro',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('link', $this->link, true);
        $criteria->compare('img', $this->img, true);
        $criteria->compare('price', $this->price, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('date', $this->date);
        $criteria->compare('category', $this->category, true);
        $criteria->compare('region', $this->region, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('metro', $this->metro, true);
        $criteria->compare('author', $this->author, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ads the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function findByHash($hash)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'hash = :hash';
        $criteria->params = array(':hash' => $hash);
        $criteria->select = 'link';
        return Ads::model()->find($criteria);
    }
}
