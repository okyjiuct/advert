<?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property integer $used
 */
class Region extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'region';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('slug, name', 'required'),
            array('used', 'numerical', 'integerOnly' => true),
            array('slug, name', 'length', 'max' => 64),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, slug, name, used', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'slug' => 'Slug',
            'name' => 'Name',
            'used' => 'Used',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('used', $this->used);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Region the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getRegion()
    {
        $region = Region::findRegion();

        if ($region !== null) {
            Region::updateRegion($region->id);

            return $region;
        } else {
            Region::updateRegion();

            $region = Region::findRegion();

            Region::updateRegion($region->id);

            return $region;
        }
    }

    public static function findRegion()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'used=0';

        return Region::Model()->find($criteria);
    }

    public static function updateRegion($id = null)
    {
        if ($id) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'id = :id';
            $criteria->params = array(':id' => $id);
            $attributes = array(
                'used' => 1
            );
            Region::model()->updateAll($attributes, $criteria);
        } else {
            $criteria = new CDbCriteria();
            $attributes = array(
                'used' => 0
            );
            Region::model()->updateAll($attributes, $criteria);
        }
    }
}
