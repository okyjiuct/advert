<?php

/**
 * This is the model class for table "query".
 *
 * The followings are the available columns in table 'query':
 * @property integer $id
 * @property string $query
 * @property string $hash
 * @property integer $date
 */
class Query extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'query';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('query, hash, date', 'required'),
            array('date', 'numerical', 'integerOnly' => true),
            array('hash', 'length', 'max' => 8),
            array('query', 'unique'),
            array('hash', 'unique'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, query, hash, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'query' => 'Query',
            'hash' => 'Hash',
            'date' => 'Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('query', $this->query, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('date', $this->date);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Query the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Добавляем новый запрос в базу
     * @param $query
     * @return bool
     */
    public static function newQuery($query)
    {
        $query = trim($query);
        $result = Query::stopWords($query);

        $countWords = explode(' ', $query);
        if (sizeof($countWords) < 2) return false;

        if ($result) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'hash = :hash';
            $criteria->params = [':hash' => Y::getHash($query)];
            $exists = Query::model()->exists($criteria);

            if ($exists) return false;

            $query = str_replace('/', '-', $query);

            $q = new Query;
            $q->query = mb_strtoupper(mb_substr($query, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr(mb_convert_case($query, MB_CASE_LOWER, 'UTF-8'), 1, mb_strlen($query), 'UTF-8');
            $q->hash = Y::getHash($query);
            $q->date = time();

            if ($q->save()) return true;
        }

        return false;
    }


    /**
     * Стоп-слова для фильтрации добавляемых запросов
     * @param $query
     * @return bool
     */
    public static function stopWords($query)
    {
        $stop = ['from', 'select', 'delete', 'update', 'union', 'left join', 'where', '%', 'like', 'продам', '?q', '??'];

        $i = 0;
        foreach ($stop as $word) {
            $pos = strripos($query, $word);
            if ($pos !== false) {
                $i++;
            }
        }

        if ($i == 0) return true;

        return false;
    }

    /**
     * Получаем список последних запросов для главной
     * @return array|CActiveRecord|CActiveRecord[]|mixed|null
     */
    public static function lastQueries()
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'query';
        $criteria->limit = '50';
        $criteria->order = 'id DESC';

        return Query::model()->cache(60)->findAll($criteria);
    }

    /**
     * Получаем список похожих запросов
     * @param $query
     * @return array
     */
    public static function relationQuery($query)
    {
        $dataProvider = new CActiveDataProvider(Query::model()->cache(60 * 60), array(
            'criteria' => array(
                'condition' => "MATCH (query) AGAINST(:keyword)",
                'params' => array(
                    ':keyword' => $query
                ),
                'offset' => 2,
                'limit' => 5
            ),
            'pagination' => false
        ));

        $data = $dataProvider->getData();

        $items = [];
        foreach ($data as $item) {
            $items[] = '<a href="/?q=' . $item->query . '">' . $item->query . '</a>';
        }

        return $items;
    }


    /**
     * Получаем и кешируем список случайных запросов
     * @param $query
     * @return bool|string
     */
    public static function randomQuery($query)
    {
        $cacheId = Y::getHash($query . '_rand');
        $random = Y::cacheGet($cacheId);

        if (!$random) {
            $criteria = new CDbCriteria();
            $criteria->limit = 5;
            $criteria->order = 'RAND()';
            $random = Query::model()->findAll($criteria);

            Y::cacheSet($cacheId, $random, 24 * 60 * 60);
        }

        if ($random) {
            $items = [];
            foreach ($random as $item) {
                $items[] = '<a class="btn btn-default btn-xs mtop12" href="/?q=' . $item->query . '">' . $item->query . '</a>';
            }

            $randomQuery = implode("&nbsp;&nbsp;", $items);

            return '<a class="btn btn-default btn-xs mtop12">Также ищут:</a>&nbsp;&nbsp;' . $randomQuery;
        }

        return false;
    }


    /**
     * Первый запрос для перелинковки
     * @return CActiveRecord
     */
    public static function firstQuery()
    {
        $first = Y::cacheGet('first');

        if (!$first) {
            $criteria = new CDbCriteria();
            $criteria->order = 'id ASC';
            $first = Query::model()->find($criteria);

            Y::cacheSet('first', $first);
        }

        return $first;
    }


    /**
     * Последний запрос для перелинковки
     * @return CActiveRecord
     */
    public static function lastQuery()
    {
        $last = Y::cacheGet('last');

        if (!$last) {
            $criteria = new CDbCriteria();
            $criteria->order = 'id DESC';
            $last = Query::model()->find($criteria);

            Y::cacheSet('last', $last);
        }

        return $last;
    }


    /**
     * Следующий запрос для перелинковки
     * @param $query
     * @return CActiveRecord
     */
    public static function prevQuery($query)
    {
        $cacheId = Y::getHash($query . '_prev');
        $prev = Y::cacheGet($cacheId);

        if (!$prev) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'query = :query';
            $criteria->params = [':query' => $query];
            $criteria->select = ['id'];
            $result = Query::model()->find($criteria);

            if (!$result) {
                $criteria = new CDbCriteria();
                $criteria->select = ['id'];
                $criteria->order = 'RAND()';
                $result = Query::model()->find($criteria);

                $id = $result->id;
            } else $id = $result->id;

            $criteria = new CDbCriteria();
            $criteria->condition = 'id < :id';
            $criteria->params = [':id' => $id];
            $criteria->order = 'id DESC';
            $prev = Query::model()->find($criteria);

            if (!$prev) $prev = Query::lastQuery($query);

            Y::cacheSet($cacheId, $prev);
        }

        return $prev;
    }


    /**
     * Следующий запрос для перелинковки
     * @param $query
     * @return CActiveRecord
     */
    public static function nextQuery($query)
    {
        $cacheId = Y::getHash($query . '_next');
        $next = Y::cacheGet($cacheId);

        if (!$next) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'query = :query';
            $criteria->params = [':query' => $query];
            $criteria->select = ['id'];
            $result = Query::model()->find($criteria);

            if (!$result) {
                $criteria = new CDbCriteria();
                $criteria->select = ['id'];
                $criteria->order = 'RAND()';
                $result = Query::model()->find($criteria);

                $id = $result->id;
            } else $id = $result->id;

            $criteria = new CDbCriteria();
            $criteria->condition = 'id > :id';
            $criteria->params = [':id' => $id];
            $criteria->order = 'id ASC';
            $next = Query::model()->find($criteria);

            if (!$next) $next = Query::firstQuery($query);

            Y::cacheSet($cacheId, $next);
        }

        return $next;
    }
}
